# Projekt-testiranje web aplikacije
## (Behavior driven testiranje u Behat-u)

### Testiranje web aplikacije „Ordinacija obiteljske medicine”

#### Popis testova:

1.	Otvaranje naslovne strane
2.	Otvaranje „O nama“ strane
3.	Otvaranje „Prijava“ strane
4.	Sadržaj stranice „Prijava“
5.	Doktor, uspješno logiranje
6.	Doktor, uspješno logiranje ˙(primjer testa koji je prošao i koji nije)
7.	Pacijent, uspješna prijava
8.	Pacijent, neuspješna prijava
9.	Doktor, uspješna prijava i provjera sadržaja 
10.	Doktor, provjera stranica
11.	Provjera sadržaja stranice  „O nama“ 
12.	Provjera sadržaja stranice  „Pacijenti“
13.	Provjera sadržaja opcije „Pretraga pacijenata“
14.	Provjera funkcionalnosti opcije „Pretraga pacijenata“, tipka „Traži“ pacijent postoji u bazi
15.	Provjera funkcionalnosti opcije „Pretraga pacijenata“, pacijent ne postoji u bazi, provjera stanja u bazi
16.	Provjera funkcionalnosti opcije „Pretraga pacijenata“, tipka „Prikaži sve“
17.	Provjera sadržaja i funkcionalnosti opcije „Brisanje pacijenata“, pacijent postoji u bazi i uspješno je obrisan
18.	Provjera sadržaja i funkcionalnosti opcije „Brisanje pacijenata“, pacijent ne postoji u bazi
19.	Provjera sadržaja i funkcionalnosti opcije „Izmjena pacijenata“, pacijent postoji u bazi i uspješno je promijenjen, stanje u bazi
20.	Stanje u bazi za opciju „Brisanje pacijenata“
21.	Provjera funkcionalnosti opcije „Registracija pacijenata“, pacijent uspješno dodan, stanje u bazi
22.	Provjera funkcionalnosti opcije „Registracija pacijenata“, pacijent nije dodan, nisu unešeni svi potrebni podatci
23.	Provjera funkcionalnosti stranice „Pitanja“, još nema unesenih pitanja
24.	Provjera funkcionalnosti stranica: „Pitanja“, „q&a“ i „Kontakt“, provjera za pacijente i doktora, stanje u bazi
25.	Provjera opcije „Odjava“ za doktora i pacijente, odjava uspješna

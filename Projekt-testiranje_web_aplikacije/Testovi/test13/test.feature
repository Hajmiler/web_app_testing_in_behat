Feature: Checking content when I click on "Pretraga pacijenta"

When I click on "Pretraga pacijenta" I should see window that provide input for
"Ime" and "Prezime" and options "Traži" and "Prikaži"

Scenario:

Given I am on "http://localhost/web/index.php"
And I follow "Prijava"
And I fill in "uname" with "ihajmiler"
And I fill in "psw" with "1234"
And I press "Prijavi se"
Then I should be on "/web/index.php"
When I follow "Pacijenti"
And I go to "http://localhost/web/display.php"
Then I should see "Ime"
And I should see "Prezime"
And I should see "Traži"
And I should see "Prikaži sve"

	
	
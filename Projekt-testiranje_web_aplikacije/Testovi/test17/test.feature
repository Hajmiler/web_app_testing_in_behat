Feature: Checking functionality when I use "Brisanje pacijenta" option

If I as a doctor go to "Brisanje pacijenata" I should be on "http://localhost/web/delete.php"
and I should see the exact components of the page and should be able to delete the patient and get 
appropriate message

Scenario:

	Given I am on "http://localhost/web/index.php"
	And I follow "Prijava"
	And I fill in "uname" with "ihajmiler"
	And I fill in "psw" with "1234"
	And I press "Prijavi se"
	Then I should be on "/web/index.php"
	When I follow "Pacijenti"
	And I go to "http://localhost/web/delete.php"
	Then I should be on "http://localhost/web/delete.php"
	And I should see "Ime"
	And I should see "Prezime"
	And I should see "Obriši"
	When I fill in "firstname" with "luka"
	And I fill in "lastname" with "lukic"
	And I press "Obriši"
	Then I should see "Pacijent je obrisan iz sustava"
	
	
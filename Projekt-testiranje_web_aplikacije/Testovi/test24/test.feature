Feature: Checking functionality of the "Pitanja", "q&a" and "Contact" page

	When I go to "Pitanja" page on the doctor's profile, if there aren't any questions asked by patients I shoud see message "Nema još pitanja." 
	and databese table "contact" should be empty, if there are questions asked then I should see those questions and I should be able to answer them 
	and see informations about patients that asked questions. When I go to "q&a" page I should see questions and answers for all patients. 
	On the other hand, signed in patient should be able to ask questions in the "Contact" page and see his/her questions and answers in the "q&a" page. 
	If there aren't any questions answered then the message "Nema još odgovorenih pitanja." should be displayed in the "q&a" page.
	For example if patient with firstname "luka" and lastname "lukic" asked question: "Kada mogu doci na pregled" I should see that question 
	and should be able to answer it, when I answer that question I should be able to see that question and answer in the "q&a" page, 
	also questions and answers related to other patients. Patient in the given example "luka" should be able to see his question 
	and answer in the "q&a" page when he sign in with his credentials.

Scenario Outline:
	Given I go to "http://localhost/web/login.php"
	When I fill in "uname" with "ihajmiler"
	And I fill in "psw" with "1234"
	And I press "Prijavi se"
	Then I should be on "http://localhost/web/index.php"
	Given I am on "http://localhost/web/questions.php"
	Then I should not see "Ime i prezime"
	And I should not see "Predmet"
	And I should not see "Upit"
	And I should not see "Odgovori na pitanje"
	And I should see "Nema još pitanja."
	
	When I go to "http://localhost/phpmyadmin/sql.php?server=1&db=ordinacija&table=contact&pos=0"
	Then I should see "<id>"
	Then I should see "<name>"
	Then I should see "<email>"
	Then I should see "<subject>"
	Then I should see "<message>"
	Then I should see "<answer>"
	
	Examples:
		|id| |name| |email| |subject| |message| |answer| 
		|  | |    | |     | |       | |       | |      |
		
Scenario:
	Given I go to "http://localhost/web/login.php"
	When I fill in "uname" with "ihajmiler"
	And I fill in "psw" with "1234"
	And I press "Prijavi se"
	Then I should be on "http://localhost/web/index.php"
	When I go to "http://localhost/web/q&a.php"
	Then I should see "Nema još odgovorenih pitanja."
	
Scenario:
	Given I go to "http://localhost/web/login.php"
	When I fill in "uname" with "luki"
	And I fill in "psw" with "5"
	And I press "Prijavi se"
	Then I should be on "http://localhost/web/index.php"
	When I go to "http://localhost/web/q&a.php"
	Then I should see "Nema još odgovorenih pitanja."
	
Scenario:
	Given I go to "http://localhost/web/login.php"
	When I fill in "uname" with "luki"
	And I fill in "psw" with "5"
	And I press "Prijavi se"
	Then I should be on "http://localhost/web/index.php"
	When I go to "http://localhost/web/contact.php"
	Then I should see "Ime i prezime"
	And I should see "E-mail"
	And I should see "Predmet"
	And I should see "Upit"
	And I should see an "#send" element
	When I fill in "name" with "luka lukic"
	And I fill in "email" with "llukic@etfos.hr"
	And I fill in "subject" with "Bol u ruci."
	And I fill in "message" with "Kada mogu doci na pregled?"
	And I press "Pošalji"
	Then I should see "Vaša poruka je poslana te možete očekivati odgovor u roku od 2 dana."
	
Scenario Outline:
	Given I go to "http://localhost/web/login.php"
	When I fill in "uname" with "ihajmiler"
	And I fill in "psw" with "1234"
	And I press "Prijavi se"
	Then I should be on "http://localhost/web/index.php"
	When I go to "http://localhost/web/questions.php"
	Then I should see "<Ime i prezime>"
	And I should see "<E-mail>"
	And I should see "<Predmet>"
	And I should see "<Upit>"
	And I should see "Odgovori na pitanje"
	
	Examples:
		|Ime i prezime||E-mail         ||Predmet    ||Upit                      |
		|luka lukic   ||llukic@etfos.hr||Bol u ruci.||Kada mogu doci na pregled?|

Scenario Outline:
	Given I go to "http://localhost/web/login.php"
	When I fill in "uname" with "ihajmiler"
	And I fill in "psw" with "1234"
	And I press "Prijavi se"
	Then I should be on "http://localhost/web/index.php"
	Given I am on "http://localhost/web/questions.php"
	When I follow "Odgovori na pitanje"
	Then I should be on "/web/doctors answer.php"
	And I should see "Odgovor"
	And I should see an "#answer1" element
	When I fill in "odgovor" with "Sutra"
	And I press "Odgovori"
	Then I should be on "http://localhost/web/q&a.php"
	When I go to "http://localhost/web/list.php"
	Then I should see "<Pitanje:>"
	And I should see "<Odgovor:>"
	
	Examples:
		|Pitanje:                  | |Odgovor:|
		|Kada mogu doci na pregled?| |Sutra   |
		
Scenario Outline:
	Given I go to "http://localhost/web/login.php"
	When I fill in "uname" with "luki"
	And I fill in "psw" with "5"
	And I press "Prijavi se"
	Then I should be on "http://localhost/web/index.php"
	When I go to "http://localhost/web/list.php"
	Then I should see "<Pitanje:>"
	And I should see "<Odgovor:>"
	
	Examples:
		|Pitanje:                  | |Odgovor:|
		|Kada mogu doci na pregled?| |Sutra   |


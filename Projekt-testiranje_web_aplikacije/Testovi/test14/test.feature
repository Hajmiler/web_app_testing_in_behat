Feature: Checking functionality when I use "Pretraga pacijenta" option

When I provide name and surname which are in the database and click on "Traži"
I should see data about given name and surname

Scenario Outline:

Given I am on "http://localhost/web/index.php"
And I follow "Prijava"
And I fill in "uname" with "ihajmiler"
And I fill in "psw" with "1234"
And I press "Prijavi se"
Then I should be on "/web/index.php"
When I follow "Pacijenti"
And I go to "http://localhost/web/display.php"
And I fill in "fname" with "luka"
And I fill in "lname" with "lukic"
And I press "Traži"
Then I should see "<OIB>"
Then I should see "<Ime>"
Then I should see "<Prezime>"
Then I should see "<Korisničko ime>"
Then I should see "<Email>"

Examples:
|OIB     | |Ime | |Prezime| |Korisničko ime| |Email          |  
|45454545| |luka| |lukic  | |luki          | |llukic@etfos.hr|
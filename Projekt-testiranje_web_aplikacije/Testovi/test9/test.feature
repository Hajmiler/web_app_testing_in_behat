Feature: Doctor, succesful login

When I as a doctor provide legitimate username and password 
I should be on "Naslovna" page and should see "Pacijenti", "Pitanja", "Q&A" and "Odjava"

Scenario:

Given I am on "http://localhost/web/login.php"
When I fill in "uname" with "ihajmiler"
And I fill in "psw" with "1234"
And I press "Prijavi se"
Then I should be on "/web/index.php"
And I should see "Pacijenti"
And I should see "Pitanja"
And I should see "Q & A"
And I should see "Odjava"
	
	
Feature: Checking the content of page "Prijava"
		
When I go to "Prijava" page I should see fields "Korisničko ime" and "Lozinka"

Scenario:

	Given I go to "http://localhost/web/login.php"
	Then I should see text matching "Korisničko ime"
	And I should see text matching "Lozinka"
	
	
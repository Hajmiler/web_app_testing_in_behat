Feature: Checking functionality of the "Pitanja" page-there aren't any questions asked

	When I go to "Pitanja" page on the doctor's profile, if there aren't any questions asked by patients, 
	then I should see message "Nema još pitanja"
	
Scenario:
	
	Given I am on "http://localhost/web/questions.php"
	Then I should see "Nema još pitanja"
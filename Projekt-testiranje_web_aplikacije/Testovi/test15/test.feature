Feature: Checking functionality when I use "Pretraga pacijenta" option

When I provide name and surname which are not in the database and click on "Traži"
I should see message "Nema rezultata za prikaz u bazi"

Scenario:

Given I am on "http://localhost/web/index.php"
And I follow "Prijava"
And I fill in "uname" with "ihajmiler"
And I fill in "psw" with "1234"
And I press "Prijavi se"
Then I should be on "/web/index.php"
When I follow "Pacijenti"
And I go to "http://localhost/web/display.php"
And I fill in "fname" with "kiki"
And I fill in "lname" with "kikic"
And I press "Traži"
Then I should see "Nema rezultata za prikaz u bazi"


	
	
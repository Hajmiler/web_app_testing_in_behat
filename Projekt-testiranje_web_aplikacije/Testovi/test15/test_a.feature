Feature: 


Scenario Outline:
	Given I am on "http://localhost/web/display.php"
	When I fill in "fname" with "kiki"
	And I fill in "lname" with "riki"
	And I press "Traži"
	Then I should see "<OIB>"
	And I should see "<Ime>"
	And I should see "<Prezime>"
	And I should see "<Korisničko ime>"
	And I should see "<E-mail>"
	And I should see "<Lozinka>"
	And I should see "Nema rezultata za prikaz u bazi"
	
	
	Examples:
		|OIB| |Ime| |Prezime| |Korisničko ime| |E-mail | |Lozinka|                          
		|   | |   | |       | |              | |       | |       |
		
	
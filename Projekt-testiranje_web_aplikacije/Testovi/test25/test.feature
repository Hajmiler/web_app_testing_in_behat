Feature: Log out when logged in as doctor and log out when logged in as patient
	When I log out either as doctor or as patient I should be on "Naslovna" page
	
Scenario:
	Given I am on "http://localhost/web/index.php"
	When I follow "Prijava"
	When I fill in "uname" with "ihajmiler"
	And I fill in "psw" with "1234"
	And I press "Prijavi se"
	Then I should be on "http://localhost/web/index.php"
	And I should see "Odjava"
	When I follow "Odjava"
	Then I should be on "http://localhost/web/index.php"
	And I should see "Prijava"
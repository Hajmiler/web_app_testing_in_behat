Feature: Checking functionality when I use "Pretraga pacijenta" option, using button "Prikaži sve"

When I as a doctor go to "Pacijenti" page and pick option "Pretraga pacijenta" and press button "Prikaži sve"
I should see all the patients that are in the database and their data

Scenario Outline:

Given I am on "http://localhost/web/index.php"
And I follow "Prijava"
And I fill in "uname" with "ihajmiler"
And I fill in "psw" with "1234"
And I press "Prijavi se"
Then I should be on "/web/index.php"
When I follow "Pacijenti"
And I go to "http://localhost/web/display.php"
And I press "Prikaži sve"

Examples:
|OIB        | |Ime | |Prezime| |Korisničko ime| |Email           |
|454545454  | |luka| |lukic  | |luki          | |llukic@etfos.hr |
|11111111111| |mara| |maric  | |mamaric       | |mmaric@gmail.com|
|67676767676| |kata| |katic  | |kkatic        | |kkatic@gmail.com|
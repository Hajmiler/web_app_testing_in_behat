Feature: Checking content of "Pacijenti" page

When I open "Pacijenti" page I should see exactly defined content

Scenario:

Given I am on "http://localhost/web/index.php"
And I follow "Prijava"
And I fill in "uname" with "ihajmiler"
And I fill in "psw" with "1234"
And I press "Prijavi se"
Then I should be on "/web/index.php"
When I follow "Pacijenti"
Then I should see "Pretraga pacijenata"
And I should see "Brisanje pacijenata"
And I should see "Izmjena Pacijenata"
And I should see "Registracija pacijenata"


	
	
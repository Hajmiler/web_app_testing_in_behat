Feature: Checking functionality of the "Registracija" option-successful registration

	When I go to "Registracija" option and provide "OIB", "Ime", "Prezime", 
	"Korisničko ime", "E-mail", "Lozinka" and "Potvrda lozinke" then I should see message 
	"Uspješno unesen pacijent" and I should see provided data in the database table "users"
	
Scenario Outline:
	
	Given I am on "http://localhost/web/registration.php"
	When I fill in "oib" with "12345678910"
	And I fill in "firstname" with "kata"
	And I fill in "lastname" with "katic"
	And I fill in "username" with "kkatic"
	And I fill in "email" with "kkatic@gmail.com"
	And I fill in "password1" with "kata"
	And I fill in "password2" with "kata"
	When I press "Potvrdi unos"
	Then I should see "Uspješno unesen pacijent"
	When I go to "http://localhost/phpmyadmin/sql.php?db=ordinacija&table=users&pos=0"
	Then I should see "<OIB>"
	Then I should see "<Ime>"
	Then I should see "<Prezime>"
	Then I should see "<Korisničko ime>"
	Then I should see "<E-mail>"
	
	Examples:
		|OIB        | |Ime  | |Prezime| |Korisničko ime| |E-mail              | 
		|12345678910| |kata | |katic  | |kkatic        | |kkatic@gmail.com    | 
Feature: Checking functionality of the "Registracija" option-unsuccessful registration

	When I go to "Registracija" option and do not provide "OIB", "Ime", "Prezime", 
	"Korisničko ime", "E-mail", "Lozinka" or "Potvrda lozinke" and I press "Potvrdi unos" 
	then I should see apropriate messages:
		"Ime nedostaje"
		"Prezime nedostaje"
		"Korisničko ime nedostaje"
		"Email nedostaje"
		"Lozinka nedostaje"
		"Morate ponoviti lozinku"
	
Scenario:
	
	Given I am on "http://localhost/web/registration.php"
	When I fill in "oib" with ""
	And I fill in "firstname" with ""
	And I fill in "lastname" with ""
	And I fill in "username" with ""
	And I fill in "email" with ""
	And I fill in "password1" with ""
	And I fill in "password2" with ""
	When I press "Potvrdi unos"
	
	Then I should see "Ime nedostaje"
	Then I should see "Prezime nedostaje"
	Then I should see "Korisničko ime nedostaje"
	Then I should see "Email nedostaje"
	Then I should see "Lozinka nedostaje"
	Then I should see "Morate ponoviti lozinku"
	

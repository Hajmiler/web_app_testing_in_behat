Feature: Checking functionality when I use "Izmjena pacijenata" option

If I as a doctor go to "Izmjena pacijenata" I should be on "http://localhost/web/update.php"
ans I shoud see particular components of the page, should be able to search for and get the 
patient i searched for and should be able to make data changes for that patient, i should see that 
changes in the database

Scenario Outline:

	Given I am on "http://localhost/web/index.php"
	And I follow "Prijava"
	And I fill in "uname" with "ihajmiler"
	And I fill in "psw" with "1234"
	And I press "Prijavi se"
	Then I should be on "/web/index.php"
	When I follow "Pacijenti"
	And I go to "http://localhost/web/update.php"
	Then I should be on "http://localhost/web/update.php"
	And I should see "Ime" 
	And I should see "Prezime"
	And I should see "Traži"
	And I should see "OIB"
	And I should see "Ime"
	And I should see "Prezime"
	And I should see "Korisničko ime"
	And I should see "E-mail"
	And I should see "Lozinka"
	And I should see "Promijeni"
	
	When I fill in "oib" with "11111111111"
	And I fill in "firstname" with "mara"
	And I fill in "lastname" with "maric"
	And I fill in "username" with "marica"
	And I fill in "email" with "mara.maric@gmail.com"
	And I press "Promijeni"
	When I go to "http://localhost/phpmyadmin/sql.php?db=ordinacija&table=users&pos=0"
	Then I should see "<OIB>"
	Then I should see "<Ime>"
	Then I should see "<Prezime>"
	Then I should see "<Korisničko ime>"
	Then I should see "<E-mail>"
	
	Examples:
		|OIB        | |Ime  | |Prezime| |Korisničko ime| |E-mail              | 
		|11111111111| |mara | |maric  | |marica        | |mara.maric@gmail.com| 
	

	
Feature: Doctor, open "Pacijenti", "Pitanja", "Q & A" and "Odjava" page

When I as a doctor provide legitimate username and password 
I should be on "Naslovna" page and should be able to open "Pacijenti", 
"Pitanja", "Q & A" and "Odjava" page

Scenario:

Given I am on "http://localhost/web/login.php"
When I fill in "uname" with "ihajmiler"
And I fill in "psw" with "1234"
And I press "Prijavi se"
Then I should be on "/web/index.php"
And I should see "Pacijenti"
When I follow "Pacijenti"
Then I should be on "http://localhost/web/patients.php"
When I follow "Pitanja"
Then I should be on "http://localhost/web/questions.php"
When I follow "Q & A"
Then I should be on "http://localhost/web/q&a.php"
When I follow "Odjava"
Then I should be on "http://localhost/web/index.php"
	
	
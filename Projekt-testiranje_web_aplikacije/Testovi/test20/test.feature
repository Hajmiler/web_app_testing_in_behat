Feature: Checking "Brisanje pacijenata" option-database situation

When i delete the patient he/she should not be in the database anymore

Scenario Outline:
	Given I am on "http://localhost/web/delete.php"
	When I fill in "firstname" with "mara"
	And I fill in "lastname" with "maric"
	And I press "Obriši"
	
	When I go to "http://localhost/phpmyadmin/sql.php?db=ordinacija&table=users&pos=0"
	Then I should not see "<oib>"
	And I should not see "<firstname>"
	And I should not see "<lastname>"
	And I should not see "<username>"
	And I should not see "<email>"
	
	Examples:
		|oib        | |firstname  | |lastname| |username| |email            | 
		|11111111111| |mara       | |maric   | |mamaric | | mmaric@gmail.com| 
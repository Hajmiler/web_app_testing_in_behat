Feature: Checking functionality when I use "Brisanje pacijenata" option

If I as doctor go to "Brisanje pacijenata" I should be on "http://localhost/web/delete.php"
and if I provide firstname and lastname that do not exist in database
then I should get message "Pacijent ne postoji"


Scenario:
	Given I am on "http://localhost/web/index.php"
	When I follow "Prijava"
	When I fill in "uname" with "ihajmiler"
	And I fill in "psw" with "1234"
	And I press "Prijavi se"
	Then I should be on "/web/index.php"
	When I follow "Pacijenti"
	And I go to "http://localhost/web/delete.php"
	Then I should be on "http://localhost/web/delete.php"
	When I fill in "firstname" with "kiki"
	And I fill in "lastname" with "riki"
	And I press "Obriši"
	Then I should see "Pacijent ne postoji"
Feature: Checking content of "O nama" page

When I open "O nama" page I should see exactly defined content

Scenario:

Given I am on "http://localhost/web/index.php"
And I follow "O nama"
Then I should see "Ordinacija je osnovana 1991. godine u Našicama i među prvim je privatnim ordinacijama u Hrvatskoj. Od samog početka osnovana je kao ordinacija Opće medicine s dijagnostikom, a tijekom godina razvila se u Specijalističku ordinaciju Obiteljske medicine s dijagnostikom koja prvenstveno njeguje obiteljski i sveobuhvatni medicinski pristup svakom pojedincu koji se obrati za pregled ili savjet."
And I should see "U ordinaciji se prvenstveno njeguje razgovor s pacijentom u kojem liječnik doznaje osobitosti njegovih zdravstvenih tegoba, ali i u kontekstu obiteljske situacije zbog eventualnih problema unutar obitelji, na poslu ili okolini pojedinca, jer sve su to mogući uzroci tegoba ili nastale bolesti."
And I should see "Njegujući takav pristup pacijentu, jedna tegoba koja je signal čovjeku zbog kojeg se javlja liječniku može ovakvim sveobuhvatnim pristupom liječnika odvesti u sasvim drugom dijagnostičkom smjeru. To drugim riječima znači da se gušenje u vratu u ovoj ordinaciji ne tretira samo kao simptom, već se traže uzroci i moguće posljedice."
And I should see "Znači, u tom smislu bit će potreban razgovor na početku susreta u ordinaciji, a tek zatim slijedi dijagnostika prema odluci liječnika i na kraju ukoliko se ne utvrdi organski uzrok npr. štitnjača, srce, pluća ili nešto drugo, potrebno je ponovno tražiti mogući uzrok tegoba, ponovnim razgovorom s pacijentom gdje se u razgovoru traže mogući uzroci tegoba."
And I should see "Dakle ordinacija iako ima mogućnost dijagnostike ultrazvučnim pregledima najsuvremenijim kolor doplerom Toshiba Aplio XG, EKG-om i drugim aparatima ne obavlja samo preglede npr. ultrazvuka trbušne šupljine već u ordinaciju pacijenti dolaze s tegobama, a liječnik nakon razgovora i uvida u situaciju svakog pacijenta određuje što bi bilo potrebno od pregleda učiniti i na kraju donosi dijagnozu bolesti ili stanja i odluku o daljnjem postupku liječenja."
And I should see "Iz ordinacije pacijenti ne odlaze samo s nalazom, već sa objašnjenjem njihovog stanja i odgovorima na sva pacijentova pitanja uz objašnjenje uzroka tegoba i načinu liječenja do kojeg se došlo razgovorom, kliničkim pregledom i dijagnostičkim pretragama u ordinaciji."
And I should see "Vama na usluzi su Ivan Hajmiler, dr. med. spec. obiteljske medicine s dugogodišnjim iskustvom, kao i medicinska sestra Ana Anić."
	
	
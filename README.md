# Projekt-testiranje web aplikacije
## (Behavior driven testiranje u Behat-u)

### Autor: Ivan Hajmiler

#### Testiranje web aplikacije „Ordinacija obiteljske medicine“

##### Alati i tehnologije:

- Behat-open source Behavior-driven development okvir za testiranje aplikacija pisanih u PHP-u (alat za testiranje ponašanja aplikacije koristeći poseban jezik Gherkin)
- XAMPP-web poslužitelj
- MySQL-baza podataka
- PHP-poslužiteljski skriptni jezik (eng. server scripting language)
- Visual Studio Code-source-code editor
- Mink-open source browser controller/emulator za web aplikacije
- Goutte-biblioteka (eng. library) za PHP skeniranje i pretraživanje zaslona. Pruža API za indeksiranje web stranica i izdvajanje podataka iz HTML / XML odgovora.
- MinkExtension-integracijski sloj između Behat-a i Mink-a
- Gherkin-linijski orijentirani jezik koji koristi uvlačenja za definiranje strukture. Služi za pisanje testova u Behat-u. Prevodi se u PHP. Pisan je da bude razumljiv čovjeku.
- Composer-instalacija svih potrebnih komponenti
- Notepad++-pisanje testova (Feature files)
- Cmd-instalacija i provođenenje testova preko windows command prompt-a

##### Testiranje:

Testovi su pisani u Gherkin jeziku unutar test.feature tekstualnih datoteka koristeći tekst editor Notepad++. Testovi se sastoje od osnovnih komponenata: Feature, Scenario i Scenario Outline. Unutar Feature komponente se stavlja objašnjenje testa odnosno što test radi (testira), a unutar Scenario i Scenario Outline komponente se piše sadržaj testa u Gherkin-u. Kada želimo koristiti neke posebne komponente testa kao što su tablice onda koristimo Scenario Outline. Tablice koristimo kada želimo na strukturiran način obrađivati veću količinu informacija. Gherkin je linijski jezik, a uvlačenja definiraju strukturu stoga svaka linija predstavlja jedan korak (eng. step) pojedinog scenarija. Moguće je izvršiti više scenarija u jednoj feature datoteci. Testovi su izvršavani u Behat-u koristeći Mink tzv. headless browser koji su pokretani preko Windows command prompt-a. Zelenom bojom su označeni scenariji i koraci koji su prošli, a crvenom koji nisu. Plavom bojom su označeni koraci koji su prošli, a dio su Scenario outline, isto tako i sam Scenario Outline. Žutom bojom su označeni nedefinirani koraci, koraci koji nisu uspješno prevedeni u PHP te trebaju definiciju. Također su istom bojom označeni i pending steps koji traže dodatnu definiciju jer nisu u potpunosti definirani. Preskočeni koraci slijede iza nedefiniranih, pending i neuspješnih koraka i označeni su svijetlo plavom bojom. Na kraju svakog testa piše ukupna statistika testa na temelju čega zaključujemo je li test prošao ili nije. PHP prevedeni kod za pojedini test nalazi se u FeatureContext datoteci. Pojedini elementi koda koji su korišteni tijekom testiranja dohvaćani su koristeći HTML i CSS identifikatore kao što su name i id. 

Testovi su razvrstani u mape, a objedinjeni su unutar mape „Testovi“. Mape sa pripadajućim testovima su poredane po brojevima.  U svakoj mapi nalazi se slika testa i pripadajuća test.feature datoteka. Unutar word dokumenta „Popis testova“ koji se također nalazi u mapi „Testovi“ nalazi se naslov svakog testa sa odgovarajućim brojevima. Izvršeno je funkcionalno testiranje web stranice koju sam izradio, a naziva se „Ordinacija obiteljske medicine“. Testovi su uspješno izvršeni i web stranica je u cijelosti testirana. Opširni opis svakog testa nalazi se unutar slika i test.feature datoteka za taj test, a i sam kod testa je opis, s obzirom da je i to jedna od bitnih karakteristika testiranja u Behat-u.

##### Opaska:

Primjerak ovog teksta spremljenog u Word-u nalazi se unutar mape "Testovi" pod nazivom "Korišteni alati i opis testiranja".
